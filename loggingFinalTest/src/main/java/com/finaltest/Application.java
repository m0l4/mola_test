package com.finaltest;

import com.finaltest.levels.LogLevelable;
import com.finaltest.levels.impl.LogLevels;
import com.finaltest.thread.ThreadTest;

public class Application{


    public static void main(String[] args) throws InterruptedException {
    	LogLevelable logs = new LogLevels();
    	logs.createLevels();
    	
    	ThreadTest test = new ThreadTest();
    	test.createThreads();
    }

}
