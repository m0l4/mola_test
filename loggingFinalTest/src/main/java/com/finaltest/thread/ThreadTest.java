package com.finaltest.thread;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class ThreadTest {
	private static final Logger LOG = LogManager.getLogger();

	public void createThreads() {
		Runnable r1 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.trace("Test trace");
		};
		Runnable r2 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.debug("Test debug");
		};
		Runnable r3 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.info("Test info");
		};
		Runnable r4 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.warn("Test warn");
		};
		Runnable r5 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.error("Test error");
		};
		Runnable r6 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.fatal("Test fatal");
		};
		Runnable r7 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.fatal("Test fatal2");
		};
		Runnable r8 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.log(Level.getLevel("MUSTFIX"),"Test MUSTFIX");
		};
		Runnable r9 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.log(Level.getLevel("DATABASE"),"Test DATABASE");
		};
		Runnable r10 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.log(Level.getLevel("FAILOVER"),"Test FAILOVER");
		};
		Runnable r11 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.log(Level.getLevel("FIXLATER"),"Test FIXLATER");
		};
		Runnable r12 = () -> {
			ThreadContext.put("name", "Adalberto Moran");
			ThreadContext.push("MarioBros");
			LOG.log(Level.getLevel("MYDEBUG"),"Test MYDEBUG");
		};
		new Thread(r1,"Thread1").start();
		new Thread(r2,"Thread2").start();
		new Thread(r3,"Thread3").start();
		new Thread(r4,"Thread4").start();
		new Thread(r5,"Thread5").start();
		new Thread(r6,"Thread6").start();
		new Thread(r7,"Thread7").start();
		new Thread(r8,"Thread8").start();
		new Thread(r9,"Thread9").start();
		new Thread(r10,"Thread10").start();
		new Thread(r11,"Thread11").start();
		new Thread(r12,"Thread12").start();
	}
}
