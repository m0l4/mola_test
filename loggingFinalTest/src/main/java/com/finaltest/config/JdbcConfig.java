package com.finaltest.config;

import java.sql.Connection;

import org.apache.commons.dbcp2.BasicDataSource;


public class JdbcConfig {
	private static BasicDataSource dataSource;
	
	private JdbcConfig() {
		
	}
	
	public static Connection getConnection() {
		if( dataSource == null) {
			dataSource = new BasicDataSource();
			dataSource.setDriverClassName("org.postgresql.Driver");
			dataSource.setUsername("postgres");
			dataSource.setPassword("postgres");
			dataSource.setUrl("jdbc:postgresql://localhost:5432/shared_code");
			
		}
		try {
			return dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}

