package com.finaltest;


import com.finaltest.levels.LogLevelable;
import com.finaltest.levels.impl.LogLevels;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p>Test all the classes of log4j2</p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FinalTest{

    private Level[] levels;

    private String[] logLevels;

    private int[] intLevels;

    private List<String> appendersList;

    @BeforeEach
    public void init(){

        LogLevelable logLevelable = new LogLevels();
        logLevelable.createLevels();
        levels = Level.values();
        logLevels = new String[]{"TRACE", "ALL", "FIXLATER", "MUSTFIX", "INFO", "DEBUG", "OFF", "WARN", "DATABASE",
                "ERROR", "MYDEBUG", "FATAL", "FAILOVER"};
        intLevels = new int[]{600, Integer.MAX_VALUE, 450, 1, 400, 500, 0, 300, 250, 200, 550, 100, 350};
        appendersList = new ArrayList<>();
        appendersList.add("console");
        appendersList.add("jsonAppender");
        appendersList.add("xmlAppender");
        appendersList.add("yamlAppender");
        appendersList.add("htmlAppender");
        appendersList.add("rollingFile");
        appendersList.add("smtpAppender");
        appendersList.add("asyncAppender");
        appendersList.add("jdbcAppender");
        appendersList.add("failAppender");

    }

    @Test
    @Order(1)
    public void testNumberLoggersLevel(){

        assertEquals(13, levels.length, "Número de loggers incorrecto");
    }

    @Test
    @Order(2)
    public void testLevelNames(){

        String[] actualLevels = new String[levels.length];
        for(int i = 0; i < levels.length; i++){
            actualLevels[i] = levels[i].name();
        }
        Assertions.assertArrayEquals(logLevels, actualLevels,
                                     "Algún nivel no está declarado o fue declarado con otro nombre"
        );
    }

    @Test
    @Order(3)
    public void testIntLevels(){

        int[] actualIntLevels = new int[levels.length];
        for(int i = 0; i < levels.length; i++){
            if(levels[i].name().equals("ALL")){
                actualIntLevels[i] = Integer.MAX_VALUE;
            }else{
                actualIntLevels[i] = levels[i].intLevel();
            }
        }
        Assertions.assertArrayEquals(intLevels, actualIntLevels,
                                     "Algún nivel no está declarado o fue declarado con otro valor"
        );
    }

    @Test
    @Order(4)
    public void testAppenders(){

        Logger logger = LogManager.getLogger();
        Map<String, Appender> appenders = ((org.apache.logging.log4j.core.Logger) logger).getAppenders();
        Set<String> keySet = appenders.keySet();
        List<String> actualAppenders = new ArrayList<>();

        for(String key : keySet){
            actualAppenders.add(key);
        }
        boolean resultAppenders = new HashSet<>(actualAppenders).equals(new HashSet<>(appendersList));
        assertEquals(true, resultAppenders,
                                "Algún appender no fue declarado con el nombre requerido o no ha sido creado aún"
        );
    }

    @Test
    @Order(5)
    public void testFiles(){
        File jsonFile = new File("jsonAppender.log");
        File xmlFile = new File("xmlAppender.log");
        File yamlFile = new File("yamlAppender.log");
        File htmlFile = new File("htmlAppender.log");
        File rollingFile = new File("rollingFile.log");

        boolean filesExist = jsonFile.exists() && xmlFile.exists() && yamlFile.exists()
                && htmlFile.exists() && rollingFile.exists();

        assertEquals(true, filesExist,
                                "Algún archivo no ha sido creado o se creó con otro nombre, revisar configuración");

    }
}
